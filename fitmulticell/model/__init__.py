"""
Model
=====

"""

from .base import MorpheusModel


__all__ = [
    'MorpheusModel'
]
