"""
FitMultiCell
============

"""

from .version import __version__  # noqa: F401
from .model import *
from .sumstat import *
from .distance import *
