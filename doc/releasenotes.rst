.. _releasenotes:

Release Notes
=============


0.0 series
----------


0.0.3 (2019-11-25)
~~~~~~~~~~~~~~~~~~

* Fix error with utf-8 encoding.
* Update notebooks to recent changes.


0.0.2 (2019-10-10)
~~~~~~~~~~~~~~~~~~

* Basic repository set up.
* Added documentation on fitmulticell.readthedocs.io.
* Implemented MorpheusModel class.
* Designed a logo.
* Added a basic usage example in doc/example.


0.0.1 (2019-09-24)
~~~~~~~~~~~~~~~~~~

Initial preparatory release, no functionality.
