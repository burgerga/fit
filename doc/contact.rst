Contact
=======

Discovered an error? Need help? Not sure if something works as intended? Please contact us!

If you think your problem could be of general interest, please consider creating an issue on gitlab, which will then also be helpful for other users: https://gitlab.com/fitmulticell/fit/issues

If you prefer to contact us via e-mail, please write to: 

* Yannik Schaelte (rather parameter estimation) `yannik.schaelte@gmail.com <yannik.schaelte@gmail.com>`_
* Joern Staruss (rather modeling) `joern.staruss@tu-dresden.de <joern.staruss@tu-dresden.de>`_

