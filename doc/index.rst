.. fitmulticell documentation master file, created by
   sphinx-quickstart on Wed Oct  9 15:52:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fitmulticell's documentation!
========================================


.. image:: https://readthedocs.org/projects/fitmulticell/badge/?version=latest
   :target: https://fitmulticell.readthedocs.io/en/latest
.. image:: https://badge.fury.io/py/fitmulticell.svg
   :target: https://badge.fury.io/py/fitmulticell


Source code: https://gitlab.com/fitmulticell/fit


.. image:: logo/logo_long.svg.png
   :alt: FitMultiCell logo
   :align: center


Fitting of multi-cellular models combining the tools `morpheus <https://morpheus.gitlab.io>`_ and `pyABC <https://github.com/icb-dcm/pyabc>`_.


.. toctree::
   :maxdepth: 2
   :caption: User's guide

   install
   example


.. toctree::
   :maxdepth: 2
   :caption: Developer's guide

   contribute
   deploy


.. toctree::
   :maxdepth: 2
   :caption: API reference

   api_model


.. toctree::
   :maxdepth: 2
   :caption: About

   releasenotes
   contact
   license
   logo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
