Install
=======


Requirements
------------

This package requires Python 3.6 or later.
It is tested on Linux using Travis continuous integration. Other platforms
(Windows, Mac) should also be usable, but are not so thoroughly tested.


Dependencies
------------

This package relies on the tools morpheus and pyABC.

* To install morpheus, checkout `<https://morpheus.gitlab.io/#download>`_.
* To install pyabc, checkout `<https://pyabc.readthedocs.io/en/latest/installation.html>`_.


Install from PIP
----------------

The package can be installedd from the Python Package Index PyPI via pip::

   pip3 install fitmulticell


Install from GIT
----------------

If you want the bleeding edge version, install directly from gitlab::

   pip3 install git+https://gitlab.com/fitmulticell/fit

If you need to have access to the source code, you can instead download it
via::

   git clone https://gitlab.com/fitmulticell/fit

and then install from the local repository via::

   cd fit
   pip3 install -e .


Upgrade
-------

If you want to upgrade an installation, replace ``install`` by
``install --upgrade`` in the above pip commands.
