# FitMultiCell

![Logo](https://gitlab.com/fitmulticell/fit/raw/master/doc/logo/logo_long.svg.png)

[![docs](https://readthedocs.org/projects/fitmulticell/badge/?version=latest)](http://fitmulticell.readthedocs.io/en/latest/)
[![pypi version](https://badge.fury.io/py/fitmulticell.svg)](https://badge.fury.io/py/fitmulticell)

Fitting of multi-cellular models combining the tools [morpheus](https://gitlab.com/morpheus.lab/morpheus) and [pyABC](https://github.com/icb-dcm/pyabc).


* **Documentation:** [fitmulticell.readthedocs.io](https://fitmulticell.readthedocs.io)
* **Contact:** [fitmulticell.readthedocs.io/en/latest/contact.html](https://fitmulticell.readthedocs.io/en/latest/contact.html)
* **Source code:** [gitlab.com/fitmulticell/fit](https://gitlab.com/fitmulticell/fit)
* **Examples:** [fitmulticell.readthedocs.io/en/latest/example.html](https://fitmulticell.readthedocs.io/en/latest/example.html)
* **Bug reports:** [gitlab.com/fitmulticell/fit/issues](https://gitlab.com/fitmulticell/fit/issues)
